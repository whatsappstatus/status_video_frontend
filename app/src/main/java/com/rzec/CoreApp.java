package com.rzec;

import android.app.Application;
import android.app.DownloadManager;
import android.content.Context;

/**
 * Created by System43 on 8/4/2017.
 */

public class CoreApp extends Application
{

    private static CoreApp instance;
    private static DownloadManager dManager;
    @Override
    public void onCreate()
    {
        instance = this;
        super.onCreate();
    }

    public static CoreApp getInstance()
    {
        return instance;
    }

    public DownloadManager getdManager() {
        if(dManager ==null)
            dManager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        return dManager;
    }
}
