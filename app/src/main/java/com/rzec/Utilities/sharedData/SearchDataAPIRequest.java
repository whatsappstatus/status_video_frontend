package com.rzec.Utilities.sharedData;

import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.rzec.CoreApp;
import com.rzec.Utilities.NetworkStatusUtils;
import com.rzec.Utilities.Toaster;
import com.rzec.services.GetJsonData;
import com.rzec.services.ThreadCode;
import com.rzec.videostatus.R;

/**
 * Created by System43 on 12/5/2017.
 */

public class SearchDataAPIRequest {

    private SearchAPIReguestListener listener;

    public SearchDataAPIRequest(Context context) {
        listener = (SearchAPIReguestListener) context;
        VideoListRequest();
    }

    private void VideoListRequest() {
        if (NetworkStatusUtils.isConnected()) {
            //AddEvent("1","Video list API request");
            String url = CoreApp.getInstance().getString(R.string.base_url) + CoreApp.getInstance().getString(R.string.tags_url);
            GetJsonData mLoginService = new GetJsonData();
            mLoginService.execute(url, ThreadCode.THREAD_VIDEO_LIST, handler);
        } else {
            Toaster.ShowMessageToast("Please check network connection");
        }
    }


    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case ThreadCode.THREAD_VIDEO_LIST:
                    final String result = (String) msg.obj;
                    listener.SuccessResponse(result);
                    break;
                case ThreadCode.FAILURE:
                    listener.FailureResponse("");
                    break;
                default:
                    break;
            }
        }

    };
    public interface SearchAPIReguestListener {
        void SuccessResponse(String result);

        void FailureResponse(String failure);
    }
}

