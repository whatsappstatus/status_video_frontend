package com.rzec.Utilities.sharedData;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.Preference;

import com.rzec.CoreApp;

/**
 * Created by System43 on 12/5/2017.
 */

public class SharedData {
    private static final String SHARED_PACKAGE = "rzec.video.com.shared";
    private static SharedData instSharedData;
    private static SharedPreferences mPrefs;
    private static SharedPreferences.Editor mEditor;

    public static synchronized SharedData getInstSharedData()
    {
        if(instSharedData == null) {
            instSharedData = new SharedData();
        }
        return instSharedData;
    }

    private SharedData()
    {
        mPrefs = CoreApp.getInstance().getSharedPreferences(SHARED_PACKAGE, Context.MODE_PRIVATE);
        mEditor = mPrefs.edit();
    }

    public void SetSearchData(String json)
    {
        mEditor.putString("serach_json",json);
        mEditor.commit();
    }
    public String GetSearchData()
    {
        return mPrefs.getString("serach_json","");
    }
}
