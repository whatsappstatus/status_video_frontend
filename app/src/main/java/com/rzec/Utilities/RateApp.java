package com.rzec.Utilities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;

import java.net.URI;

/**
 * Created by System43 on 12/22/2017.
 */

public class RateApp {

    private final static String APP_TITLE = "Video Status";
    private final static String APP_PACKAGE = "com.rzec.videostatus";

    public static void RateApplication(final Context context)
    {
        Dialog dialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        String message = "If you enjoy using "+APP_TITLE+", Please take a moment to rate the app.";
        builder.setTitle(APP_TITLE)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("RATE NOW", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        try
                        {
                            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id="+APP_PACKAGE)));
                        }
                        catch (ActivityNotFoundException e)
                        {
                            Toaster.ShowMessageToast("Application not found in play store");
                        }
                    }
                })
                .setNegativeButton("LATER", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        dialog = builder.create();
        dialog.show();
    }
}
