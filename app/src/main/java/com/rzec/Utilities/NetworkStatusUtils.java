package com.rzec.Utilities;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.rzec.CoreApp;

/**
 * Created by System43 on 1/29/2016.
 */
public class NetworkStatusUtils
{
    public static boolean isConnected()
    {
        ConnectivityManager connectivity = (ConnectivityManager) CoreApp.getInstance().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null)
        {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED)
                    {
                        return true;
                    }

        }
        return false;
    }
}
