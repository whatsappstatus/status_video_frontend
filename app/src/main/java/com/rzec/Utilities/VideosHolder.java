package com.rzec.Utilities;

import com.rzec.domain.VideoDomain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by System43 on 11/29/2017.
 */

public class VideosHolder
{
    private static VideosHolder instance;

    private HashMap<String, VideoDomain> videosMap;
    public static synchronized VideosHolder getInstance()
    {
        if(instance==null)
            instance = new VideosHolder();
        return instance;
    }

    public void AddAllVideo(List<VideoDomain> array)
    {
        if(videosMap == null)
            videosMap = new HashMap<>();
        for (VideoDomain domain : array)
        {
            videosMap.put(domain.getId(),domain);
        }
    }

    public VideoDomain GetVideoDomain(String key)
    {
        return videosMap != null ? videosMap.get(key) : null;
    }

    public Map<String,VideoDomain> GetVideoArray()
    {
        return videosMap;
    }
}
