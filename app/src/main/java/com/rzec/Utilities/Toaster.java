package com.rzec.Utilities;

import android.widget.Toast;

import com.rzec.CoreApp;


/**
 * Created by System43 on 8/4/2017.
 */

public class Toaster
{
    public static void ShowMessageToast(String message)
    {
        Toast.makeText(CoreApp.getInstance(), message, Toast.LENGTH_SHORT).show();
    }
}
