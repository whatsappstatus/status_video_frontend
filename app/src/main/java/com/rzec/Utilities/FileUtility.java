package com.rzec.Utilities;

import android.os.Environment;

import java.io.File;

/**
 * Created by System43 on 11/1/2017.
 */

public class FileUtility
{
    public static File GetVideoFolder()
    {
        File videoFolder = new File(Environment.getExternalStorageDirectory() + File.separator + "RzecVideoStatus");
        if (!videoFolder.exists()) {
            videoFolder.mkdirs();
        }
        return videoFolder;
    }

    public static String CreateFolder()
    {
        File videoFolder = new File(Environment.getExternalStorageDirectory() + File.separator + "RzecVideoStatus");
        if (!videoFolder.exists()) {
            videoFolder.mkdirs();
        }
        return "RzecVideoStatus";
    }
}
