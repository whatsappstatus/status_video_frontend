package com.rzec.Utilities;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import com.rzec.CoreApp;

/**
 * Created by System43 on 11/7/2017.
 */

public class DisplayMatricUtilities
{
    private static DisplayMatricUtilities instance;
    private static int height = 0;
    private static int width = 0;

    public static synchronized DisplayMatricUtilities getInstance()
    {
        if(instance == null)
            instance = new DisplayMatricUtilities();
        return instance;
    }

    private DisplayMatricUtilities()
    {
        WindowManager windowManager = (WindowManager) CoreApp.getInstance().getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        height = displayMetrics.heightPixels;
        width = displayMetrics.widthPixels;
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }
}
