package com.rzec.Utilities.gestures;


import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.ImageButton;

/**
 * Created by System43 on 11/13/2017.
 */


public class GestureImageView extends ImageButton {
    public GestureDetector gestureDetector;
    private OnDoubleClickListener onDoubleClickListener;
    public GestureImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        gestureDetector = new GestureDetector(context, new GestureListener());
    }
    public void SetOnDoubleClick(OnDoubleClickListener listener)
    {
        this.onDoubleClickListener = listener;
    }

    @Override
    public boolean performClick() {
        return super.performClick();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return gestureDetector.onTouchEvent(event);
    }
    private class GestureListener extends GestureDetector.SimpleOnGestureListener {
        @Override
        public boolean onDown(MotionEvent e) {
            return true;
        }

        // event when double tap occurs
        @Override
        public boolean onDoubleTap(MotionEvent e) {
            onDoubleClickListener.onDoubleTab();
            return true;
        }

        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            onDoubleClickListener.onSingleTab();
            return super.onSingleTapConfirmed(e);
        }
    }


    public interface OnDoubleClickListener
    {
        void onDoubleTab();
        void onSingleTab();
    }
}
