package com.rzec.download;

import android.app.DownloadManager;
import android.net.Uri;

import com.rzec.CoreApp;
import com.rzec.Utilities.FileUtility;
import com.rzec.domain.VideoDomain;

import java.util.HashMap;

/**
 * Created by System43 on 11/9/2017.
 */

public class DownloadManagerUtils {

    public static HashMap<Long,String> mDownLoadArrayQueue = new HashMap<>();
    public static void DownloadVideo(String url, VideoDomain domain)
    {
        Uri Download_Uri = Uri.parse(url);
        DownloadManager.Request request = new DownloadManager.Request(Download_Uri);
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE);
        request.setAllowedOverRoaming(false);
        request.setTitle( domain.getName());
        request.setDescription(domain.getLanguage() + " | " + domain.getTag());
        request.setVisibleInDownloadsUi(true);
        request.setDestinationInExternalPublicDir(FileUtility.CreateFolder(), "/"+domain.getFileName());
        domain.setFilePath(FileUtility.GetVideoFolder()+"/"+domain.getFileName());
        Long refid = CoreApp.getInstance().getdManager().enqueue(request);
        mDownLoadArrayQueue.put(refid,domain.getId());
    }
}
