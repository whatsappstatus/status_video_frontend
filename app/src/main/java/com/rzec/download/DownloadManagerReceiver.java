package com.rzec.download;

import android.app.DownloadManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.widget.RemoteViews;

import com.rzec.CoreApp;
import com.rzec.Utilities.Toaster;
import com.rzec.Utilities.VideosHolder;
import com.rzec.domain.VideoDomain;
import com.rzec.videostatus.R;


/**
 * Created by System43 on 11/9/2017.
 */

public class DownloadManagerReceiver extends BroadcastReceiver {

    private String mTitle = "";
    private String mFilePath = "";

    @Override
    public void onReceive(Context context, Intent intent) {
        // get the refid from the download manager
        long referenceId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);

        GetDownloadedPath(referenceId);         //method to get title, path, description

        Intent intent1 = new Intent(Intent.ACTION_VIEW);
        Uri uri = Uri.parse(mFilePath);
        intent1.setDataAndType(uri, "video/*");
        PendingIntent pendingIntent = PendingIntent.getActivity(context, (int) referenceId, Intent.createChooser(intent1, "Choose Player"), PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(CoreApp.getInstance())
                        .setAutoCancel(true)
                        .setContentIntent(pendingIntent);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            // build a complex notification, with buttons and such
            //
            mBuilder.setContent(getComplexNotificationView());
        } else {
            mBuilder.setContentTitle(mTitle)
                    .setContentText("Download completed")
                    .setContentInfo(mFilePath);
        }
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBuilder.setSmallIcon(R.drawable.notification_white);
        } else {
            mBuilder.setSmallIcon(R.mipmap.ic_launcher);
        }


        NotificationManager notificationManager = (NotificationManager) CoreApp.getInstance().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify((int) referenceId, mBuilder.build());

        VideoDomain domain = VideosHolder.getInstance().GetVideoDomain(DownloadManagerUtils.mDownLoadArrayQueue.get(referenceId));
        if(domain!=null)
            domain.setFileDownloaded(true);

        Toaster.ShowMessageToast("Download completed");

        // remove it from our list
        DownloadManagerUtils.mDownLoadArrayQueue.remove(referenceId);
    }

    private void GetDownloadedPath(long downId) {
        DownloadManager.Query q = new DownloadManager.Query();
        q.setFilterById(downId);
        Cursor c = CoreApp.getInstance().getdManager().query(q);

        if (c.moveToFirst()) {
            int status = c.getInt(c.getColumnIndex(DownloadManager.COLUMN_STATUS));
            if (status == DownloadManager.STATUS_SUCCESSFUL) {
                mFilePath = c.getString(c.getColumnIndex("local_uri"));
                mTitle = c.getString(c.getColumnIndex(DownloadManager.COLUMN_TITLE));
            }
        }
    }

    private RemoteViews getComplexNotificationView() {
        // Using RemoteViews to bind custom layouts into Notification
        RemoteViews notificationView = new RemoteViews(
                CoreApp.getInstance().getPackageName(),
                R.layout.notification_layout
        );
        notificationView.setTextViewText(R.id.notification_title, mTitle);
        notificationView.setTextViewText(R.id.file_location, mFilePath);
        return notificationView;
    }
}
