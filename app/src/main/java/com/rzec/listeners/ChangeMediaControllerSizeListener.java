package com.rzec.listeners;

/**
 * Created by System43 on 12/14/2017.
 */

public interface ChangeMediaControllerSizeListener
{
    void VideoSizeChanged(int width, int height);
}
