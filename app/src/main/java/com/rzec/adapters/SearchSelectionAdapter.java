package com.rzec.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.rzec.domain.SearchItem;
import com.rzec.videostatus.R;

import java.util.ArrayList;

public class SearchSelectionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<SearchItem> mSelectedArrays;
    private final OnSelectedItemClickListener mListener;

    public SearchSelectionAdapter(Context context) {
        this.mListener = (OnSelectedItemClickListener) context;
        mSelectedArrays = new ArrayList<>();
    }

    public int getArraySize() {
        return mSelectedArrays == null ? 0 : mSelectedArrays.size();
    }

    public ArrayList<SearchItem> GetSelectedArray() {
        return mSelectedArrays;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        viewHolder = getViewHolder(parent, inflater);
        return viewHolder;
    }

    @NonNull
    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
        RecyclerView.ViewHolder viewHolder;
        View v1 = inflater.inflate(R.layout.selection_search_item, parent, false);
        viewHolder = new ViewHolder(v1);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder1, int position) {
        final SearchItem domain = mSelectedArrays.get(holder1.getAdapterPosition());
        final ViewHolder holder = (ViewHolder) holder1;
        holder.mSelectedItem.setText(domain.getSearchTitle());
        holder.mSelectedItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.RemoveSelection(domain);
            }
        });
    }


    @Override
    public int getItemCount() {
        return mSelectedArrays == null ? 0 : mSelectedArrays.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    /*
   Helpers
   _________________________________________________________________________________________________
    */

    public void add(SearchItem mc) {
        if (mSelectedArrays.size() > 0) {
            for (SearchItem item : mSelectedArrays) {
                if (mc.getSearchTag().equals(item.getSearchTag())) {
                    mSelectedArrays.remove(item);
                    break;
                }
            }
        }
        mSelectedArrays.add(mc);
        notifyDataSetChanged();
    }

    public void remove(SearchItem item) {
        mSelectedArrays.remove(item);
        notifyDataSetChanged();
    }

    public SearchItem getItem(int position) {
        return mSelectedArrays.get(position);
    }


   /*
   View Holders
   _________________________________________________________________________________________________
    */

    /**
     * Main list's content ViewHolder
     */
    public class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView mSelectedItem;

        private ViewHolder(View view) {
            super(view);
            mSelectedItem = (TextView) view.findViewById(R.id.search_item);
        }
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnSelectedItemClickListener {
        void RemoveSelection(SearchItem domain);
    }

}
