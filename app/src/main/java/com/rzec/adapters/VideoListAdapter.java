package com.rzec.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.rzec.CoreApp;
import com.rzec.GlideApp;
import com.rzec.Utilities.DisplayMatricUtilities;
import com.rzec.Utilities.NetworkStatusUtils;
import com.rzec.Utilities.Toaster;
import com.rzec.domain.VideoDomain;
import com.rzec.videostatus.R;

import java.io.File;
import java.util.ArrayList;

public class VideoListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM = 0;
    private static final int LOADING = 1;

    private ArrayList<VideoDomain> mVideoArrays;
    private Context context;

    private boolean isLoadingAdded = false;
    private final OnVideoItemClickListener mListener;

    public VideoListAdapter(Context context) {
        this.context = context;
        this.mListener = (OnVideoItemClickListener) context;
        mVideoArrays = new ArrayList<>();
    }

    public ArrayList<VideoDomain> getMovies() {
        return mVideoArrays;
    }

    public void setMovies(ArrayList<VideoDomain> movies) {
        this.mVideoArrays = movies;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case ITEM:
                viewHolder = getViewHolder(parent, inflater);
                break;
            case LOADING:
                View v2 = inflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new LoadingVH(v2);
                break;
        }
        return viewHolder;
    }

    @NonNull
    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
        RecyclerView.ViewHolder viewHolder;
        View v1 = inflater.inflate(R.layout.video_list_item, parent, false);
        viewHolder = new VideoViewHolder(v1);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder1, int position) {
        switch (getItemViewType(position)) {
            case ITEM:
                final VideoDomain domain = mVideoArrays.get(holder1.getAdapterPosition());
                final VideoViewHolder holder = (VideoViewHolder) holder1;
                holder.mVideoTitle.setText(domain.getName());
                holder.mVideoViews.setText(String.valueOf(domain.getViews()));
                holder.mVideoTags.setText(domain.getGenre());
                ChangeDownloadButton(domain.isFileDownloaded(), holder.mDownloadBTN);
                //holder.mDownloadBTN.setBackgroundResource(!domain.isFileDownloaded() ? R.drawable.dl_btn_outline_normal : R.drawable.dl_btn_outline_selected);
                //holder.mDownloadBTN.setVisibility(!domain.isFileDownloaded() ? View.VISIBLE : View.INVISIBLE);
                GlideApp.with(holder.mVideoImage.getContext())
                        .load(domain.getImage_url())
                        .override(DisplayMatricUtilities.getInstance().getWidth(), holder.mVideoImage.getHeight()) // resizes the image to 100x200 pixels but does not respect aspect ratio
                        .centerCrop() // scale to fill the ImageView and crop any extra
                        .listener(new RequestListener<Drawable>() {
                            @Override
                            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                return false;
                            }
                        })
                        .into(holder.mVideoImage);

                holder.mPlayBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (null != mListener) {
                            // Notify the active callbacks interface (the activity, if the
                            // fragment is attached to one) that an item has been selected.
                            mListener.DoubleTab(domain);
                        }
                    }
                });

                /*holder.mShareBTN.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (null != mListener) {
                            // Notify the active callbacks interface (the activity, if the
                            // fragment is attached to one) that an item has been selected.
                            mListener.ShareVideo(domain);
                        }
                    }
                });*/
                holder.mDownloadBTN.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (!isDownloadedFileExist(domain))
                            if (!domain.isDownloadInProgress())
                                if (null != mListener && NetworkStatusUtils.isConnected()) {
                                    // Notify the active callbacks interface (the activity, if the
                                    // fragment is attached to one) that an item has been selected.
                                    domain.setFileDownloaded(false);
                                    mListener.DownloadVideo(holder.mDownloadBTN, domain);
                                } else {
                                    Toaster.ShowMessageToast("Please check network connection");
                                }
                            else
                                Toaster.ShowMessageToast("Video download is in progress");
                        else
                            Toaster.ShowMessageToast("File Already Downloaded");
                    }
                });
                break;
            case LOADING:
//                Do nothing
                break;
        }
    }


    @Override
    public int getItemCount() {
        return mVideoArrays == null ? 0 : mVideoArrays.size();
    }

    @Override
    public int getItemViewType(int position) {
        return (position == mVideoArrays.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

    /*
   Helpers
   _________________________________________________________________________________________________
    */

    public void add(VideoDomain mc) {
        mVideoArrays.add(mc);
        notifyItemInserted(mVideoArrays.size() - 1);
    }

    public void addAll(ArrayList<VideoDomain> mcList) {
        for (VideoDomain mc : mcList) {
            add(mc);
        }
    }

    public void remove(VideoDomain city) {
        int position = mVideoArrays.indexOf(city);
        if (position > -1) {
            mVideoArrays.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public boolean isEmpty() {
        return getItemCount() == 0;
    }


    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new VideoDomain());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;
        if (mVideoArrays.size() > 0) {
            int position = mVideoArrays.size() - 1;
            VideoDomain item = getItem(position);

            if (item != null) {
                mVideoArrays.remove(position);
                notifyItemRemoved(position);
            }
        }
    }

    public VideoDomain getItem(int position) {
        return mVideoArrays.get(position);
    }


   /*
   View Holders
   _________________________________________________________________________________________________
    */

    /**
     * Main list's content ViewHolder
     */
    public class VideoViewHolder extends RecyclerView.ViewHolder {
        private final TextView mVideoTitle, mVideoViews, mVideoTags, mDownloadBTN;
        private final AppCompatImageView mVideoImage;
        private final ImageButton mPlayBtn;
        //private final CircleImageView mDownloadBTN;

        private VideoViewHolder(View view) {
            super(view);
            mVideoTitle = (TextView) view.findViewById(R.id.video_name);
            mVideoViews = (TextView) view.findViewById(R.id.video_views);
            mVideoImage = (AppCompatImageView) view.findViewById(R.id.video_image);
            mPlayBtn = (ImageButton) view.findViewById(R.id.video_play_btn);
            //mShareBTN = (CircleImageView) view.findViewById(R.id.share_btn);
            //mDownloadBTN = (CircleImageView) view.findViewById(R.id.download_video_btn);
            mVideoTags = (TextView) view.findViewById(R.id.video_tags);
            mDownloadBTN = (TextView) view.findViewById(R.id.download_video_btn);
        }
    }


    protected class LoadingVH extends RecyclerView.ViewHolder {

        public LoadingVH(View itemView) {
            super(itemView);
        }
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnVideoItemClickListener {
        void ShareVideo(VideoDomain domain);

        void DownloadVideo(TextView downloadBTN, VideoDomain domain);

        void DoubleTab(VideoDomain domain);
    }

    public void ChangeDownloadButton(boolean success, TextView dBTN) {
        dBTN.setCompoundDrawablesWithIntrinsicBounds(success ? CoreApp.getInstance().getResources().getDrawable(R.drawable.ic_check_white_24dp) : CoreApp.getInstance().getResources().getDrawable(R.drawable.ic_file_download), null, null, null);
        dBTN.setTextColor(success ? CoreApp.getInstance().getResources().getColor(R.color.colorWhite) : CoreApp.getInstance().getResources().getColor(R.color.colorPrimary));
        dBTN.setBackgroundResource(success ? R.drawable.dl_btn_outline_pressed : R.drawable.dl_btn_outline_normal);
        dBTN.setText(success ?"Downloaded":"download");
    }

    private boolean isDownloadedFileExist(VideoDomain domain)
    {
        return domain.isFileDownloaded() && isFileExist(domain);
    }

    private boolean isFileExist(VideoDomain domain)
    {
        return new File(domain.getFilePath()).exists();
    }
}
