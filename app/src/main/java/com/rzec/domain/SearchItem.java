package com.rzec.domain;

/**
 * Created by System43 on 12/4/2017.
 */

public class SearchItem {

    private String searchTag;
    private String searchTitle;

    public String getSearchTag() {
        return searchTag;
    }

    public void setSearchTag(String searchTag) {
        this.searchTag = searchTag;
    }

    public String getSearchTitle() {
        return searchTitle;
    }

    public void setSearchTitle(String searchTitle) {
        this.searchTitle = searchTitle;
    }
}
