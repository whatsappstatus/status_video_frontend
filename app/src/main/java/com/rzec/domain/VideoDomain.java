package com.rzec.domain;

import com.rzec.listeners.DownloadCompleteListener;

import java.io.Serializable;

/**
 * Created by System43 on 11/8/2017.
 */

public class VideoDomain implements Serializable{

    private String id;
    private String url;
    private String name;
    private String fileName;
    private boolean isFileDownloaded;
    private boolean isDownloadInProgress;
    private String createdDate;
    private String tag;
    private int views;
    private String language;
    private String filePath;
    private String credits;
    private String actor;
    private String genre;
    private String image_url;
    private String movie_name;
    private DownloadCompleteListener completeListener = null;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public boolean isFileDownloaded() {
        return isFileDownloaded;
    }

    public void setFileDownloaded(boolean fileDownloaded) {
        isFileDownloaded = fileDownloaded;
        if(fileDownloaded && completeListener !=null) {
            completeListener.DownloadComplete();
            completeListener = null;
            isDownloadInProgress = false;
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getCredits() {
        return credits;
    }

    public void setCredits(String credits) {
        this.credits = credits;
    }

    public String getActor() {
        return actor;
    }

    public void setActor(String actor) {
        this.actor = actor;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getMovie_name() {
        return movie_name;
    }

    public void setMovie_name(String movie_name) {
        this.movie_name = movie_name;
    }

    public void setCompleteListener(DownloadCompleteListener completeListener) {
        this.completeListener = completeListener;
    }

    public boolean isDownloadInProgress() {
        return isDownloadInProgress;
    }

    public void setDownloadInProgress(boolean downloadInProgress) {
        isDownloadInProgress = downloadInProgress;
    }
}
