package com.rzec.services;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class PostTokenService extends AsyncTask<Object, Integer, String>
{
	private String Url;
	private String jsonObject;
	private Integer threadCode;
	private Handler handler;
	@SuppressWarnings("unchecked")
	@Override
	protected String doInBackground(Object... params)
	{
		Url = (String)params[0];
		jsonObject = (String)params[1];
		threadCode = (Integer)params[2];
		handler = (Handler)params[3];
		
        String result = "";
        StringBuffer response = new StringBuffer();
		HttpURLConnection con = null;
        try {
 
        	URL obj = new URL(Url);
			con = (HttpURLConnection) obj.openConnection();
			con.setConnectTimeout(StandardCodes.httpTimeOut);
			
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json; charset=utf8");
			//con.setRequestProperty("Accept", "application/json");
			//con.setRequestProperty("Method", "POST");
	 
			con.setUseCaches(false);
			con.setDoInput(true);
			con.setDoOutput(true);
			con.setDoOutput(true);
			OutputStream outputStream = con.getOutputStream();
			outputStream.write(jsonObject.getBytes());
			outputStream.flush();
			outputStream.close();

			int HttpResult = con.getResponseCode();
				BufferedReader in = new BufferedReader(
						new InputStreamReader(con.getInputStream()));
				String inputLine;

				while ((inputLine = in.readLine()) != null) {
					if (isCancelled()) {
						break;
					} else {
						response.append(inputLine);
					}
				}
				in.close();
				result = response.toString();
				if (isCancelled()) {

				} else {
					Message message = new Message();
					message.obj = result;
					message.what = threadCode;
					handler.sendMessage(message);
				}

        } 
        catch (Exception e)
        {
        	if(isCancelled())
			{

			}
        	else {
				handler.sendEmptyMessage(StandardCodes.FAILURE);
			}
        }
		return "success";
	}
}
