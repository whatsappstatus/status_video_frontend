package com.rzec.services;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;

import org.apache.http.NameValuePair;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class GetJsonData extends AsyncTask<Object, Integer, String> {

    public InputStream is;
    public String Url;
    public Integer threadCode;
    private Handler handler;

    public static final String REQUEST_METHOD = "GET";
    public static final int READ_TIMEOUT = 15000;
    public static final int CONNECTION_TIMEOUT = 15000;

    @SuppressWarnings("unchecked")
    @Override
    protected String doInBackground(Object... params) {
        Url = (String) params[0];
        threadCode = (Integer) params[1];
        handler = (Handler) params[2];
        try {
            //Create a URL object holding our url
            URL myUrl = new URL(Url);
            //Create a connection
            HttpURLConnection connection = (HttpURLConnection)
                    myUrl.openConnection();
            //Set methods and timeouts
            connection.setRequestMethod(REQUEST_METHOD);
            connection.setReadTimeout(READ_TIMEOUT);
            connection.setConnectTimeout(CONNECTION_TIMEOUT);

            //Connect to our url
            connection.connect();
            //Create a new InputStreamReader
            InputStreamReader streamReader = new
                    InputStreamReader(connection.getInputStream());
            //Create a new buffered reader and String Builder
            BufferedReader reader = new BufferedReader(streamReader);
            StringBuilder stringBuilder = new StringBuilder();
            //Check if the line we are reading is not null
            String inputLine;
            while ((inputLine = reader.readLine()) != null) {
                stringBuilder.append(inputLine);
            }
            //Close our InputStream and Buffered reader
            reader.close();
            streamReader.close();
            //Set our result equal to our stringBuilder
            inputLine = stringBuilder.toString();

            if (!isCancelled()) {
                Message message = new Message();
                message.obj = inputLine;
                message.what = threadCode;
                handler.sendMessage(message);
            }
            return "success";
        } catch (Exception e) {
            if (isCancelled()) {

            } else {
                Message message = new Message();
                message.obj = e.getLocalizedMessage();
                message.what = StandardCodes.FAILURE;
                handler.sendMessage(message);
            }
            return "";
        }
    }

}
