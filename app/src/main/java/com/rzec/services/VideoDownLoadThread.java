package com.rzec.services;

import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.rzec.Utilities.Toaster;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by System43 on 11/1/2017.
 */

public class VideoDownLoadThread extends AsyncTask<Object, Integer, String>
{

    private ProgressDialog dialog;

    @Override
    protected String doInBackground(Object... objects)
    {
        try {
            dialog = (ProgressDialog) objects[2];
            URL u = new URL((String)objects[0]);
            URLConnection conn = u.openConnection();
            int contentLength = conn.getContentLength();

            InputStream is = conn.getInputStream();
            BufferedInputStream inStream = new BufferedInputStream(is, 1024 * 5);
            FileOutputStream outStream = new FileOutputStream((File)objects[1]);
            byte[] buff = new byte[5 * 1024];

            //Read bytes (and store them) until there is nothing more to read(-1)
            int len;
            long total = 0;
            while ((len = inStream.read(buff)) != -1)
            {
                total += len;
                publishProgress((int)((total*100)/contentLength));
                outStream.write(buff,0,len);
            }

            outStream.flush();
            outStream.close();
            inStream.close();
        } catch(FileNotFoundException e) {
            return null; // swallow a 404
        } catch (IOException e) {
            return null; // swallow a 404
        }
        return "success";
    }
    @Override
    protected void onPostExecute(String s)
    {
        if(s!=null)
            Toaster.ShowMessageToast("s");
        dialog.dismiss();
        super.onPostExecute(s);
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);
        dialog.setProgress(values[0]);
    }
}
