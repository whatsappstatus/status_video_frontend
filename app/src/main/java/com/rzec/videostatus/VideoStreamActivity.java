package com.rzec.videostatus;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.rzec.CoreApp;
import com.rzec.Utilities.DisplayMatricUtilities;
import com.rzec.Utilities.NetworkStatusUtils;
import com.rzec.Utilities.Toaster;
import com.rzec.Utilities.VideosHolder;
import com.rzec.domain.VideoDomain;
import com.rzec.download.DownloadManagerUtils;
import com.rzec.listeners.ChangeMediaControllerSizeListener;
import com.rzec.listeners.DownloadCompleteListener;
import com.rzec.services.GetJsonData;
import com.rzec.services.ThreadCode;

import java.io.File;

public class VideoStreamActivity extends AppCompatActivity implements
        MediaPlayer.OnCompletionListener, MediaPlayer.OnPreparedListener,
        SurfaceHolder.Callback, VideoControllerView.MediaPlayerControl, MediaPlayer.OnVideoSizeChangedListener, MediaPlayer.OnErrorListener, DownloadCompleteListener {


    private MediaPlayer mMediaPlayer;
    private SurfaceView mPreview;
    private VideoControllerView controller;
    private SurfaceHolder holder;
    private ProgressBar mProgress;
    private LinearLayout mNetworkErrorLayout;
    private VideoDomain mDomain;
    private boolean isFullScreen = false;
    private GetJsonData serviceRequest;
    private Toolbar toolbar;

    private ActionBar mActionBar;
    private MenuItem mDownloadMenuItem;
    private  TextView mDownloadBTN;
    private ChangeMediaControllerSizeListener mControllerListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_video_stream);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mDomain = VideosHolder.getInstance().GetVideoDomain(getIntent().getStringExtra(VideoListActivity.VIDEO_KEY));

        setSupportActionBar(toolbar);
        mActionBar = getSupportActionBar();
        mActionBar.setDisplayHomeAsUpEnabled(true);
        mPreview = (SurfaceView) findViewById(R.id.surface_view);
        mProgress = (ProgressBar) findViewById(R.id.progress);
        mNetworkErrorLayout = (LinearLayout) findViewById(R.id.network_error);
        TextView videoTitle = (TextView)findViewById(R.id.video_name);
        mDownloadBTN = (TextView)findViewById(R.id.download_video_btn);
        holder = mPreview.getHolder();
        holder.addCallback(this);
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        controller = new VideoControllerView(this);
        mControllerListener = controller;
        controller.setActionBar(mActionBar);
        if(mDomain!=null) {
            videoTitle.setText(mDomain.getName());
            ChangeDownloadButton(mDomain.isFileDownloaded());
        }
    }

    private void playVideo() {
        if(mDomain!=null)
        try {
            ShowProgress(true);
            // Create a new media player and set the listeners
            mMediaPlayer = new MediaPlayer();
            mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            mMediaPlayer.setDataSource(mDomain.isFileDownloaded() ? isFileExist()?mDomain.getFilePath():mDomain.getUrl() : mDomain.getUrl());
            mMediaPlayer.setScreenOnWhilePlaying(true);
            mMediaPlayer.prepareAsync();
            mMediaPlayer.setOnPreparedListener(this);
            //mMediaPlayer.setOnVideoSizeChangedListener(this);
            mMediaPlayer.setOnErrorListener(this);

        } catch (Exception e) {
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        controller.show();
        return false;
    }


    public void onCompletion(MediaPlayer arg0) {
    }

    public void onPrepared(MediaPlayer mediaplayer) {
        if (mActionBar.isShowing())
            mActionBar.hide();
        mPreview.setBackgroundDrawable(null);
        controller.setMediaPlayer(this);
        mMediaPlayer.setDisplay(holder);
        controller.setAnchorView((FrameLayout) findViewById(R.id.videoSurfaceContainer));
        ChangeMediaControllerSize();
        mediaplayer.seekTo(100);
        ShowProgress(false);
        start();
        AddViewCountRequest();
    }

    public void surfaceChanged(SurfaceHolder surfaceholder, int i, int j, int k) {

    }

    public void surfaceDestroyed(SurfaceHolder surfaceholder) {
    }

    public void surfaceCreated(SurfaceHolder holder) {
        if(mMediaPlayer != null) {
            mMediaPlayer.setDisplay(holder);
            int seconds = mMediaPlayer.getCurrentPosition();
            mMediaPlayer.seekTo(seconds-1000);
        }

        if (mMediaPlayer == null)
            playVideo();
    }

    @Override
    protected void onPause() {
        super.onPause();
       /* if (mMediaPlayer != null && mMediaPlayer.isPlaying()) {
            pause();
        }*/
        //releaseMediaPlayer();
        pause();
    }

     @Override
     protected void onResume() {
         super.onResume();
             //start();
     }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        mDomain.setCompleteListener(null);
        releaseMediaPlayer();
        if (serviceRequest != null && serviceRequest.getStatus() == AsyncTask.Status.RUNNING)
            serviceRequest.cancel(true);
    }

    private void releaseMediaPlayer() {
        if (mMediaPlayer != null) {
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }

    @Override
    public int getDuration() {
        return mMediaPlayer != null ? mMediaPlayer.getDuration() : 0;
    }

    @Override
    public int getCurrentPosition() {
        return mMediaPlayer != null ? mMediaPlayer.getCurrentPosition() : 0;
    }

    @Override
    public void pause() {
        if (mMediaPlayer != null)
            mMediaPlayer.pause();
    }

    @Override
    public void seekTo(int i) {
        mMediaPlayer.seekTo(i);
    }

    @Override
    public void start() {
        if (mMediaPlayer != null)
            mMediaPlayer.start();
        else
            playVideo();
    }

    @Override
    public boolean isPlaying() {
        return mMediaPlayer != null && mMediaPlayer.isPlaying();
    }

    @Override
    public int getBufferPercentage() {
        return 0;
    }

    @Override
    public boolean canPause() {
        return true;
    }

    @Override
    public boolean canSeekBackward() {
        return true;
    }

    @Override
    public boolean canSeekForward() {
        return true;
    }

    @Override
    public boolean isFullScreen() {
        return isFullScreen;
    }

    @Override
    public void toggleFullScreen() {
        int orientation = getRequestedOrientation();
        if (orientation == ActivityInfo.SCREEN_ORIENTATION_PORTRAIT || orientation == ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        //ChangeMediaControllerSize();
        isFullScreen = !isFullScreen;
    }

    private int getWidthSize() {
        return mMediaPlayer.getVideoWidth() > DisplayMatricUtilities.getInstance().getWidth() ? DisplayMatricUtilities.getInstance().getHeight() : mMediaPlayer.getVideoWidth() + 50;
    }

    private int getHeightSize() {
        return (DisplayMatricUtilities.getInstance().getHeight()/2)-100;
    }

    private void ChangeMediaControllerSize()
    {
        int orientation = getRequestedOrientation();
        if(orientation == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE ) {
            toolbar.setTitle(mDomain.getName());
            toolbar.setSubtitle(mDomain.getActor() + " | " + mDomain.getGenre());
            toolbar.setBackgroundResource(R.color.colorBlackTransparent);
            holder.setFixedSize(DisplayMatricUtilities.getInstance().getHeight(), DisplayMatricUtilities.getInstance().getWidth());
            mControllerListener.VideoSizeChanged(DisplayMatricUtilities.getInstance().getHeight(), DisplayMatricUtilities.getInstance().getWidth());
        }
        else {
            toolbar.setTitle("");
            toolbar.setSubtitle("");
            toolbar.setBackgroundResource(android.R.color.transparent);
            holder.setFixedSize(DisplayMatricUtilities.getInstance().getWidth(), getHeightSize());
            mControllerListener.VideoSizeChanged(DisplayMatricUtilities.getInstance().getWidth(), getHeightSize());
        }
    }
    @Override
    public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
        if (width == 0 || height == 0) {
            return;
        }
        holder.setFixedSize(width, height);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        ChangeMediaControllerSize();
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.download_item:
                if (!isDownloadedFileExist())
                    if (!mDomain.isDownloadInProgress()) {
                        mDomain.setDownloadInProgress(true);
                        mDomain.setCompleteListener(!mDomain.isFileDownloaded() ? this : null);
                        DownloadManagerUtils.DownloadVideo(mDomain.getUrl(), mDomain);
                        mDownloadMenuItem.setTitle("Downloading");
                    }
                    else
                        Toaster.ShowMessageToast("Video download is in progress");
                else
                    Toaster.ShowMessageToast("File Already Downloaded");
                break;
            case android.R.id.home:
                Intent intent = new Intent(this, VideoListActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
                finish();
                break;
            case R.id.share_item:
                if (mDomain.isFileDownloaded()) {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    Uri uri = Uri.parse(mDomain.getFilePath());
                    sendIntent.putExtra(Intent.EXTRA_STREAM, uri);
                    sendIntent.setPackage("com.whatsapp");
                    sendIntent.setType("videos/*");
                    startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.send_to)));
                } else {
                    Toaster.ShowMessageToast("Download file before sharing");
                }
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean isDownloadedFileExist()
    {
        return mDomain.isFileDownloaded() && isFileExist();
    }

    private boolean isFileExist()
    {
        return new File(mDomain.getFilePath()).exists();
    }
    private void ShowProgress(boolean isShow) {
        mProgress.setVisibility(isShow ? View.VISIBLE : View.GONE);
    }

    private void ShowNetworkLayout(boolean isShow) {
        mNetworkErrorLayout.setVisibility(isShow ? View.VISIBLE : View.GONE);
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        //playVideo();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        /*getMenuInflater().inflate(R.menu.video_stream_menu, menu);
        mDownloadMenuItem = menu.findItem(R.id.download_item);
        ChangeDownloadButton(mDomain.isFileDownloaded());*/
        return super.onCreateOptionsMenu(menu);
    }

    private void AddViewCountRequest() {
        if (NetworkStatusUtils.isConnected()) {
            String url = getString(R.string.base_url) + getString(R.string.view_count_url);
            serviceRequest = new GetJsonData();
            url = url + "/" + mDomain.getId();
            serviceRequest.execute(url, ThreadCode.THREAD_VIDEO_COUNT, handler);
        } else {
            Toaster.ShowMessageToast("Please check network connection");
        }
    }


    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case ThreadCode.THREAD_VIDEO_COUNT:
                    final String result = (String) msg.obj;
                    ProcessVideoListResponse(result);
                    break;
                case ThreadCode.FAILURE:
                    if (!serviceRequest.isCancelled())
                        AddViewCountRequest();
                    break;
                default:
                    break;
            }
        }
    };

    private void ProcessVideoListResponse(String result) {
        try {
            if (result != null && Boolean.valueOf(result)) {
                mDomain.setViews(mDomain.getViews() + 1);
                setResult(RESULT_OK);
            }
        } catch (Exception e) {

        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, VideoListActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
        super.onBackPressed();
    }

    public void DownloadVideo(View v)
    {
        if (!isDownloadedFileExist())
            if (!mDomain.isDownloadInProgress()) {
            if(NetworkStatusUtils.isConnected()) {
                mDomain.setDownloadInProgress(true);
                mDomain.setCompleteListener(!mDomain.isFileDownloaded() ? this : null);
                DownloadManagerUtils.DownloadVideo(mDomain.getUrl(), mDomain);
                mDownloadBTN.setBackgroundResource(R.drawable.dl_btn_outline_pressed);
                mDownloadBTN.setText("Downloading");
                mDownloadBTN.setTextColor(CoreApp.getInstance().getResources().getColor(R.color.colorWhite));
                mDownloadBTN.setCompoundDrawablesWithIntrinsicBounds(CoreApp.getInstance().getResources().getDrawable(R.drawable.ic_check_white_24dp), null, null, null);
            }
            else
                Toaster.ShowMessageToast("Please check internet connection");
        }
            else
                Toaster.ShowMessageToast("Video download is in progress");
        else
            Toaster.ShowMessageToast("File Already Downloaded");
    }

    public void ChangeDownloadButton(boolean success) {
        mDownloadBTN.setCompoundDrawablesWithIntrinsicBounds(success ? CoreApp.getInstance().getResources().getDrawable(R.drawable.ic_check_white_24dp) : CoreApp.getInstance().getResources().getDrawable(R.drawable.ic_file_download), null, null, null);
        mDownloadBTN.setTextColor(success ? CoreApp.getInstance().getResources().getColor(R.color.colorWhite) : CoreApp.getInstance().getResources().getColor(R.color.colorPrimary));
        mDownloadBTN.setBackgroundResource(success ? R.drawable.dl_btn_outline_pressed : R.drawable.dl_btn_outline_normal);
        mDownloadBTN.setText(success ? "Downloaded" : "download");
    }

    @Override
    public void DownloadComplete() {
        ChangeDownloadButton(true);
    }
}
