package com.rzec.videostatus;

import android.app.DownloadManager;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.rzec.CoreApp;
import com.rzec.Utilities.FileUtility;
import com.rzec.Utilities.NetworkStatusUtils;
import com.rzec.Utilities.PaginationScrollListener;
import com.rzec.Utilities.RateApp;
import com.rzec.Utilities.Toaster;
import com.rzec.Utilities.VideosHolder;
import com.rzec.adapters.VideoListAdapter;
import com.rzec.domain.VideoDomain;
import com.rzec.download.DownloadManagerReceiver;
import com.rzec.download.DownloadManagerUtils;
import com.rzec.listeners.DownloadCompleteListener;
import com.rzec.services.GetJsonData;
import com.rzec.services.PostTokenService;
import com.rzec.services.ThreadCode;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

public class VideoListActivity extends AppCompatActivity implements VideoListAdapter.OnVideoItemClickListener, DownloadCompleteListener {

    public static final String VIDEO_KEY = "video_id";
    public static final int PLAY_REQUEST_CODE = 100;
    public static final int SEARCH_REQUEST_CODE = 101;
    private RecyclerView mListView;
    private ViewFlipper mViewFlipper;

    public static DownloadManagerReceiver mReceiver = null;

    private FirebaseAnalytics mFirebaseAnalytics;

    private VideoListAdapter mVideoAdapter;
    private LinearLayoutManager mLinearLayoutManager;
    private static Intent mSearchIntent = null;

    private static final int PAGE_START = 0;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private int TOTAL_PAGES = 0;
    private int currentPage = PAGE_START;
    private static boolean isSearchPage = false;
    private static String requestURL = "";

    private GetJsonData mVideoRequest;
    private PostTokenService mSearchRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_list);
        mListView = (RecyclerView) findViewById(R.id.list_video);
        mViewFlipper = (ViewFlipper) findViewById(R.id.vf_flipper);
        AddDecoder();
        //SetListData(getIntent().getStringExtra(LIST_KEY));
        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mReceiver = new DownloadManagerReceiver();
        registerReceiver(mReceiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
        SetListAdapter();
        VideoListRequest(true);
    }
    private void AddDecoder() {
        DividerItemDecoration itemDecorator = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        itemDecorator.setDrawable(ContextCompat.getDrawable(this, R.drawable.list_item_divider));
        mListView.addItemDecoration(itemDecorator);
    }

    private void SetListAdapter() {
        mLinearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mListView.setLayoutManager(mLinearLayoutManager);
        mListView.setItemAnimator(new DefaultItemAnimator());
        mVideoAdapter = new VideoListAdapter(this);
        mListView.setAdapter(mVideoAdapter);
        mListView.addOnScrollListener(new PaginationScrollListener(mLinearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;
                if (!isSearchPage)
                    VideoListRequest(false);
                else
                    SearchRequest(false);
            }

            @Override
            public int getTotalPageCount() {
                return TOTAL_PAGES;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
    }

    private void VideoListRequest(boolean isFirst) {
        if (NetworkStatusUtils.isConnected()) {
            if (isFirst)
                mViewFlipper.setDisplayedChild(1);
            String url = getString(R.string.base_url) + getString(R.string.video_list_url);
            mVideoRequest = new GetJsonData();
            requestURL = url + "?page=" + currentPage + "&size=5&sort=createdDate,desc";
            mVideoRequest.execute(requestURL, ThreadCode.THREAD_VIDEO_LIST, handler);
        } else {
            mViewFlipper.setDisplayedChild(2);
            Toaster.ShowMessageToast("Please check network connection");
        }
    }


    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case ThreadCode.THREAD_VIDEO_LIST:
                    final String result = (String) msg.obj;
                    ProcessVideoListResponse(result);
                    break;
                case ThreadCode.FAILURE:
                    Toaster.ShowMessageToast("unable to connect check your  network connection");
                    mViewFlipper.setDisplayedChild(2);
                    break;
                default:
                    break;
            }
        }
    };

    private void ProcessVideoListResponse(String result) {
        mVideoAdapter.removeLoadingFooter();
        if (result != null) {
            try {
                JSONObject resultObject = new JSONObject(result);
                JSONArray videoArray = resultObject.getJSONArray("content");
                if (videoArray.length() > 0) {
                    ArrayList<VideoDomain> mVideoArray = new ArrayList<>();
                    for (int i = 0; i < videoArray.length(); i++) {
                        VideoDomain domain = new VideoDomain();
                        JSONObject object = videoArray.getJSONObject(i);
                        domain.setId(object.getString("id"));
                        domain.setUrl(object.getString("url"));
                        domain.setName(object.getString("name"));
                        domain.setCreatedDate(object.getString("createdDate"));
                        domain.setViews(object.getInt("views"));
                        domain.setTag(object.getString("tag"));
                        domain.setLanguage(object.getString("language"));
                        domain.setCredits(object.getString("credits"));
                        domain.setActor(object.getString("actor"));
                        domain.setGenre(object.getString("genre"));
                        domain.setImage_url(object.getString("image_url"));
                        domain.setMovie_name(object.getString("movie_name"));
                        domain.setFileName(CreateString(domain.getId()));
                        domain.setFileDownloaded(isFileExist(domain.getFileName()));
                        domain.setFilePath(domain.isFileDownloaded() ? FileUtility.GetVideoFolder() + "/" + domain.getFileName() : null);
                        mVideoArray.add(domain);
                    }
                    VideosHolder.getInstance().AddAllVideo(mVideoArray);                                //Adding all videos to hashmap
                    mVideoAdapter.addAll(mVideoArray);
                    TOTAL_PAGES = resultObject.getInt("totalPages");
                    mViewFlipper.setDisplayedChild(0);
                } else {
                    mViewFlipper.setDisplayedChild(3);
                    //GotoSearchPage();
                    //Toaster.ShowMessageToast("Videos not found for your search");
                }

                isLoading = false;
                if (currentPage < (TOTAL_PAGES - 1)) mVideoAdapter.addLoadingFooter();
                else {
                    isLastPage = true;
                    isSearchPage = false;
                }
            } catch (JSONException e) {
                mViewFlipper.setDisplayedChild(2);
                e.toString();
            }
        }
    }

    @Override
    public void ShareVideo(VideoDomain domain) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, domain.getUrl());
        sendIntent.setType("text/plain");
        startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.send_to)));
    }

    @Override
    public void DownloadVideo(TextView downloadBTN, VideoDomain domain) {
        domain.setDownloadInProgress(true);
        domain.setCompleteListener(!domain.isFileDownloaded() ? this : null);
        DownloadManagerUtils.DownloadVideo(domain.getUrl(), domain);
        downloadBTN.setBackgroundResource(R.drawable.dl_btn_outline_pressed);
        downloadBTN.setText("Downloading");
        downloadBTN.setTextColor(CoreApp.getInstance().getResources().getColor(R.color.colorWhite));
        downloadBTN.setCompoundDrawablesWithIntrinsicBounds(CoreApp.getInstance().getResources().getDrawable(R.drawable.ic_check_white_24dp), null, null, null);
    }

    private boolean isFileExist(String fileName) {
        File[] folder = FileUtility.GetVideoFolder().listFiles();
        if (folder != null) {
            for (File file : folder) {
                if (file.getName().equals(fileName))
                    return true;
            }
        }
        return false;
    }

    @Override
    public void DoubleTab(VideoDomain domain) {
        Intent intent = new Intent(this, VideoStreamActivity.class);
        intent.putExtra(VIDEO_KEY, domain.getId());
        startActivityForResult(intent, PLAY_REQUEST_CODE);

    }

    private String CreateString(String name) {
        return "Rzec_" + name + ".MP4";
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mReceiver);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.video_search_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.search_item) {
            GotoSearchPage(null);
        }
        else if(item.getItemId() == R.id.review_app)
        {
            RateApp.RateApplication(this);
        }
        return super.onOptionsItemSelected(item);
    }

    public void GotoSearchPage(View v) {
        isSearchPage = true;
        Intent intent = new Intent(this, SearchableActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivityForResult(intent, SEARCH_REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLAY_REQUEST_CODE && resultCode == RESULT_OK) {
            mVideoAdapter.notifyDataSetChanged();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent != null && intent.getStringExtra("sort_order") != null) {
            mSearchIntent = intent;
            mVideoAdapter.clear();
            currentPage = PAGE_START;
            TOTAL_PAGES = 0;
            isLastPage = false;
            SearchRequest(true);
        }
    }

    private void SearchRequest(boolean isFirst) {
        if(mSearchIntent != null && NetworkStatusUtils.isConnected()) {
            String url = getString(R.string.base_url) + getString(R.string.search_url);
            requestURL = url + "?page=" + currentPage + "&size=5&sort=" + mSearchIntent.getStringExtra("sort_order");
            mSearchRequest = new PostTokenService();
            mSearchRequest.execute(requestURL, mSearchIntent.getStringExtra("param"), ThreadCode.THREAD_VIDEO_LIST, handler);
            if (isFirst)
                mViewFlipper.setDisplayedChild(1);
        }
        else
            Toaster.ShowMessageToast("Please check network connection");
    }

    @Override
    public void DownloadComplete() {
        mVideoAdapter.notifyDataSetChanged();
    }

    public void RequestRetry(View v) {
        if (!isSearchPage)
            VideoListRequest(true);
        else
            SearchRequest(true);
    }

    @Override
    public void onBackPressed() {
        this.finishAffinity();
        super.onBackPressed();
    }
}
