package com.rzec.videostatus;

import android.content.Intent;
import android.os.Handler;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorListener;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.rzec.Utilities.sharedData.SearchDataAPIRequest;
import com.rzec.Utilities.sharedData.SharedData;

public class WelcomActivity extends AppCompatActivity implements SearchDataAPIRequest.SearchAPIReguestListener{

    private static int STARTUP_DELAY = 200;
    private static int ANIM_ITEM_DURATION = 1000;
    private FirebaseAnalytics mFirebaseAnalytics;
    private WelcomActivity mWelcomeActivity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_welcom);
        mWelcomeActivity = this;
        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        getSpannableText();
        AnimateLogo();
        /*String searchData = SharedData.getInstSharedData().GetSearchData();
        if(searchData.equalsIgnoreCase(""))
            new SearchDataAPIRequest(this);
        else
            thread();*/
        //VideoListRequest();
    }

    private void AnimateLogo()
    {
        ViewCompat.animate(findViewById(R.id.app_logo))
                .translationY(-350)
                .setStartDelay(0)
                .setListener(new ViewPropertyAnimatorListener() {
                    @Override
                    public void onAnimationStart(View view) {
                        findViewById(R.id.app_logo).setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(View view) {

                    }

                    @Override
                    public void onAnimationCancel(View view) {

                    }
                })
                .setDuration(ANIM_ITEM_DURATION).setInterpolator(
                new DecelerateInterpolator(1.2f)).start();
        findViewById(R.id.app_name).setVisibility(View.VISIBLE);
        AppNameLogoAnim();
    }

    private void AppNameLogoAnim()
    {
        ViewCompat.animate(findViewById(R.id.app_name))
                .translationY(-350)
                .setStartDelay(STARTUP_DELAY)
                .setListener(new ViewPropertyAnimatorListener() {
                    @Override
                    public void onAnimationStart(View view) {
                        findViewById(R.id.app_name).setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(View view) {
                        String searchData = SharedData.getInstSharedData().GetSearchData();
                        if(searchData.equalsIgnoreCase(""))
                            new SearchDataAPIRequest(mWelcomeActivity);
                        else
                            thread();
                    }

                    @Override
                    public void onAnimationCancel(View view) {

                    }
                })
                .setDuration(ANIM_ITEM_DURATION).setInterpolator(
                new DecelerateInterpolator(1.2f)).start();
    }
    /*
        Setting different color to "Trending Search" text
     */
    private void getSpannableText() {
        SpannableStringBuilder builder = new SpannableStringBuilder();

        String title = "powered by ";
        SpannableString Spannable = new SpannableString(title);
        Spannable.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorBlack)), 0, title.length(), 0);
        builder.append(Spannable);

        String title1 = "R";
        SpannableString Spannable1 = new SpannableString(title1);
        Spannable1.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorPrimary)), 0, title1.length(), 0);
        builder.append(Spannable1);

        String title2 = "Zec";
        SpannableString Spannable2 = new SpannableString(title2);
        Spannable2.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.colorOrangeYellow)), 0, title2.length(), 0);
        builder.append(Spannable2);

        ((TextView) findViewById(R.id.powered_text)).setText(builder,TextView.BufferType.SPANNABLE);

    }
    private void thread()
    {
        Handler handler = new Handler();

        final Runnable r = new Runnable() {
            public void run() {
                Intent intent = new Intent(getApplicationContext(),VideoListActivity.class);
                startActivity(intent);
                finish();
            }
        };

        handler.postDelayed(r, 3000);
    }

    private void AddEvent(String id, String name)
    {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, id);
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, name);
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
    }

    @Override
    public void SuccessResponse(String result) {
        SharedData.getInstSharedData().SetSearchData(result);
        thread();
    }

    @Override
    public void FailureResponse(String failure) {

    }
}
