package com.rzec.videostatus;

import android.content.Intent;
import android.graphics.Point;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.rzec.Utilities.DisplayMatricUtilities;
import com.rzec.Utilities.NetworkStatusUtils;
import com.rzec.Utilities.Toaster;
import com.rzec.Utilities.sharedData.SearchDataAPIRequest;
import com.rzec.Utilities.sharedData.SharedData;
import com.rzec.adapters.SearchSelectionAdapter;
import com.rzec.domain.SearchItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

public class SearchableActivity extends AppCompatActivity implements View.OnClickListener, SearchSelectionAdapter.OnSelectedItemClickListener , SearchDataAPIRequest.SearchAPIReguestListener {

    private LinearLayout mTagsLayout;
    private RelativeLayout mInputSearchLayout;
    private EditText mCommonSearch;
    private Spinner mFilterSpinner;
    private RecyclerView mHorizontalList;
    private SearchSelectionAdapter mSelectedAdapter;
    private String mSearchTag = "CO";
    private String mSortOrder = "createdDate,desc";
    private String searchData = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_searchable);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setElevation(0);
        searchData = SharedData.getInstSharedData().GetSearchData();
        initViews();
        if(searchData.equalsIgnoreCase(""))
            new SearchDataAPIRequest(this);
    }

    private void initViews() {
        mTagsLayout = (LinearLayout) findViewById(R.id.search_layout);
        mInputSearchLayout = (RelativeLayout) findViewById(R.id.input_search_layout);
        mCommonSearch = (EditText) findViewById(R.id.common_search);
        mCommonSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId == EditorInfo.IME_ACTION_SEARCH)
                {
                    IntentToPreviousClass();
                }
                return false;
            }
        });
        mFilterSpinner = (Spinner) findViewById(R.id.filter_spinner);
        thread();
        UpdateSpinner();
        intHorizontalList();
    }

    private void thread()
    {
        Handler handler = new Handler();

        final Runnable r = new Runnable() {
            public void run() {
                initSearchViews();
            }
        };

        handler.postDelayed(r, 1000);
    }
    private void intHorizontalList() {
        mHorizontalList = (RecyclerView) findViewById(R.id.tags_list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mHorizontalList.setLayoutManager(layoutManager);
        mSelectedAdapter = new SearchSelectionAdapter(this);
        mHorizontalList.setAdapter(mSelectedAdapter);
    }

    private void UpdateSpinner() {
        final String[] filters = getResources().getStringArray(R.array.filters);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.search_item, filters) {

            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                TextView text = (TextView) super.getView(position, convertView, parent);
                text.setText(filters[position]);
                return text;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                TextView text = (TextView) super.getView(position, convertView, parent);
                text.setText(filters[position]);
                return text;
            }
        };
        mFilterSpinner.setAdapter(adapter);
    }



    /*
        performs XML onclick when user click on search button
     */
    public void SubmitSearch(View v) {
        IntentToPreviousClass();
    }

    private void IntentToPreviousClass() {

        if (NetworkStatusUtils.isConnected()) {
            mSearchTag = "";
            Intent intent1 = new Intent(this, VideoListActivity.class);
            intent1.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            intent1.putExtra("param", CreateParams());
            intent1.putExtra("sort_order", getSortOrder());
            mSearchTag = "CO";
            mSortOrder = "createdDate,desc";
            startActivity(intent1);
        } else {
            Toaster.ShowMessageToast("Please check internet Connection");
        }
    }

    private String CreateParams() {
        try {
            JSONObject object = new JSONObject();
            object.put("actor", "");
            object.put("tag", getTags());
            object.put("genre", getGenre());
            object.put("movie_name", "");
            object.put("general_query", mCommonSearch.getText().toString());
            object.put("searchBy", mSearchTag.equalsIgnoreCase("")?"CO":SortSearch());
            return object.toString();
        } catch (JSONException e) {
            return "";
        }
    }

    private String SortSearch()
    {
        char[] chars = mSearchTag.toCharArray();
        Arrays.sort(chars);
        return new String(chars);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    int viewLength = DisplayMatricUtilities.getInstance().getWidth();
    int remainingWidth = 0;
    LinearLayout row = null;
    ArrayList<TextView> tagsArray;
    ArrayList<TextView> genreArray;
    ArrayList<TextView> filterArray;

    private void initSearchViews() {
        tagsArray = new ArrayList<>();
        genreArray = new ArrayList<>();
        filterArray = new ArrayList<>();
        try
        {
            JSONObject object = new JSONObject(searchData);

            if(object.has("tags"))
            {
                AddTags("T",object.getJSONArray("tags"));
            }
            if(object.has("genre"))
            {
                CreateTitle("Genre");
                AddTags("G",object.getJSONArray("genre"));
            }
            CreateTitle("Filter By");
            JSONArray actorArray = new JSONArray();
            actorArray.put("Newest");
            actorArray.put("Oldest");
            AddTags("F", actorArray);
        }
        catch(JSONException e)
        {
            Toaster.ShowMessageToast("Error in json parsing");
        }

    }

    private void AddTags(String tag, JSONArray array) throws JSONException{
        for (int i = 0; i < array.length(); i++) {
            SearchItem item = new SearchItem();
            item.setSearchTag(tag);
            item.setSearchTitle(array.getString(i));
            View view = CreateItem(item, array.getString(i));
            if (i == 0 && remainingWidth == 0) {
                CreateRow();
                row.addView(view);
            } else {
                remainingWidth = measureWidth(row);
                int viewLength1 = measureWidth(view);
                remainingWidth = remainingWidth + viewLength1;
                if ((viewLength - remainingWidth) >= viewLength1) {
                    row.addView(view);
                } else {
                    CreateRow();
                    row.addView(view);
                }
            }
        }
        remainingWidth = 0;
    }


    private void CreateRow() {
        row = new LinearLayout(this);
        row.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        row.setOrientation(LinearLayout.HORIZONTAL);
        row.setPadding(5, 5, 5, 5);
        mTagsLayout.addView(row);
    }

    private View CreateItem(SearchItem tag, String value) {
        TextView text = new TextView(this);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(5, 5, 5, 5);
        text.setTextSize(14);
        setIds(tag, text);                                                                           //Setting ids and tags based on type
        text.setTextColor(getResources().getColor(R.color.colorBlue));
        text.setBackgroundResource(R.drawable.search_btn_normal);
        text.setText(value);
        text.setPadding(10, 8, 8, 10);
        text.setClickable(true);
        text.setLayoutParams(params);
        text.setOnClickListener(this);
        text.setTag(false);
        AddViewsToArray(tag.getSearchTag(), text);                                                   //Adding view to array
        return text;
    }

    private void CreateTitle(String value) {
        TextView text = new TextView(this);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.CENTER_HORIZONTAL;
        params.setMargins(5, 5, 5, 5);
        text.setTextSize(16);
        text.setTextColor(getResources().getColor(R.color.colorBlack));
        text.setText(value);
        text.setPadding(8, 8, 8, 8);
        text.setLayoutParams(params);
        text.setTypeface(null, Typeface.BOLD);
        mTagsLayout.addView(text);
    }

    private int measureWidth(View view) {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        view.measure(size.x, size.y);
        return view.getMeasuredWidth();
    }


    private void setIds(SearchItem key, View view) {
        switch (key.getSearchTag()) {
            case "T":
                view.setId(R.id.tag_id);
                view.setTag(R.id.tag_id, key);
                break;
            case "G":
                view.setId(R.id.genre_id);
                view.setTag(R.id.genre_id, key);
                break;
            case "F":
                view.setId(R.id.filter_id);
                view.setTag(R.id.filter_id, key);
                break;
        }
    }

    private void AddViewsToArray(String key, TextView view) {
        switch (key) {
            case "T":
                tagsArray.add(view);
                break;
            case "G":
                genreArray.add(view);
                break;
            case "F":
                filterArray.add(view);
                break;
        }
    }

    @Override
    public void onClick(View v) {
        TextView text = (TextView) v;
        if (text.getId() == R.id.tag_id) {
            TagSelections(text);
        } else if (text.getId() == R.id.genre_id) {
            GenreSelections(text);
        } else if (text.getId() == R.id.filter_id) {
            FilterSelections(text);
        }
        searchViewChange();
    }

    private void TagSelections(TextView view) {
        if ((Boolean) view.getTag()) {
            view.setTag(false);
            view.setBackgroundResource(R.drawable.search_btn_normal);
            view.setTextColor(getResources().getColor(R.color.colorBlue));
            mSearchTag = "CO";
            mSelectedAdapter.remove((SearchItem) view.getTag(view.getId()));
        } else {
            invalidateTagViews();
            view.setTag(true);
            view.setBackgroundResource(R.drawable.search_btn_pressed);
            view.setTextColor(getResources().getColor(R.color.colorWhite));
            mSelectedAdapter.add((SearchItem) view.getTag(view.getId()));
        }
    }

    private void GenreSelections(TextView view) {
        if ((Boolean) view.getTag()) {
            view.setTag(false);
            view.setBackgroundResource(R.drawable.search_btn_normal);
            view.setTextColor(getResources().getColor(R.color.colorBlue));
            mSearchTag = "CO";
            mSelectedAdapter.remove((SearchItem) view.getTag(view.getId()));
        } else {
            invalidateGenreViews();
            view.setTag(true);
            view.setBackgroundResource(R.drawable.search_btn_pressed);
            view.setTextColor(getResources().getColor(R.color.colorWhite));
            mSelectedAdapter.add((SearchItem) view.getTag(view.getId()));
        }
    }

    private void FilterSelections(TextView view) {
        if ((Boolean) view.getTag()) {
            view.setTag(false);
            view.setBackgroundResource(R.drawable.search_btn_normal);
            view.setTextColor(getResources().getColor(R.color.colorBlue));
            mSearchTag = "CO";
            mSelectedAdapter.remove((SearchItem) view.getTag(view.getId()));
        } else {
            invalidateFilterViews();
            view.setTag(true);
            view.setBackgroundResource(R.drawable.search_btn_pressed);
            view.setTextColor(getResources().getColor(R.color.colorWhite));
            mSelectedAdapter.add((SearchItem) view.getTag(view.getId()));
        }
    }

    private void invalidateTagViews() {
        for (TextView view : tagsArray) {
            view.setTag(false);
            view.setBackgroundResource(R.drawable.search_btn_normal);
            view.setTextColor(getResources().getColor(R.color.colorBlue));
        }
    }

    private void invalidateGenreViews() {
        for (TextView view : genreArray) {
            view.setTag(false);
            view.setBackgroundResource(R.drawable.search_btn_normal);
            view.setTextColor(getResources().getColor(R.color.colorBlue));
        }
    }

    private void invalidateFilterViews() {
        for (TextView view : filterArray) {
            view.setTag(false);
            view.setBackgroundResource(R.drawable.search_btn_normal);
            view.setTextColor(getResources().getColor(R.color.colorBlue));
        }
    }

    private void searchViewChange() {
        if (mSelectedAdapter.getArraySize() == 0) {
            mInputSearchLayout.setVisibility(View.VISIBLE);
            mHorizontalList.setVisibility(View.GONE);
            mSearchTag = "CO";
        } else {
            mCommonSearch.setText("");
            mSearchTag = "";
            mInputSearchLayout.setVisibility(View.GONE);
            mHorizontalList.setVisibility(View.VISIBLE);
        }
    }

    public void ClearSearchData(View v)
    {
        mCommonSearch.setText("");
    }
    @Override
    public void RemoveSelection(SearchItem domain) {
        switch (domain.getSearchTag()) {
            case "T":
                invalidateTagViews();
                break;
            case "G":
                invalidateGenreViews();
                break;
            case "F":
                invalidateFilterViews();
                break;
        }
        mSelectedAdapter.remove(domain);
        searchViewChange();
    }


    private String getGenre() {
        for (SearchItem item : mSelectedAdapter.GetSelectedArray()) {
            if (item.getSearchTag().equalsIgnoreCase("G")) {
                mSearchTag = mSearchTag + item.getSearchTag();
                return item.getSearchTitle();
            }
        }
        return "";
    }

    private String getTags() {
        for (SearchItem item : mSelectedAdapter.GetSelectedArray()) {
            if (item.getSearchTag().equalsIgnoreCase("T")) {
                mSearchTag = mSearchTag + item.getSearchTag();
                return item.getSearchTitle();
            }
        }
        return "";
    }

    private String getSortOrder() {
        for (SearchItem item : mSelectedAdapter.GetSelectedArray()) {
            if (item.getSearchTag().equalsIgnoreCase("F")) {
                return mSortOrder = item.getSearchTitle().equalsIgnoreCase("Newest") ? mSortOrder : "createdDate,asc";
            }
        }
        return mSortOrder;
    }

    @Override
    public void SuccessResponse(String result) {
        SharedData.getInstSharedData().SetSearchData(result);
        searchData = result;
        initSearchViews();
    }

    @Override
    public void FailureResponse(String failure) {

    }
}
